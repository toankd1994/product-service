package com.bank.product.repository;

import com.bank.product.entity.ProductEvent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ProductEventRepository extends JpaRepository<ProductEvent, UUID> {
}