package com.bank.product.service;

import com.bank.product.entity.Product;
import com.bank.product.entity.ProductEvent;
import com.bank.product.model.ProductLimitUpdateRequest;
import com.bank.product.repository.ProductEventRepository;
import com.bank.product.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class ProductService {
    @Autowired
    private ProductEventService productEventService;
    @Autowired
    private ProductRepository productRepository;

    public List<Product> retrieveProducts() {
        return productRepository.findAll(
                Sort.by(Sort.Direction.ASC, "updatedAt")
        );
    }

    @Transactional
    public Optional<Product> updateProductLimit(ProductLimitUpdateRequest request) {
        Instant updatedTime = Instant.now();
        Optional<Product> productOptional = productRepository.findProductByProductCode(request.getProductCode());
        if (!productOptional.isPresent()) {
            return productOptional;
        }
        Product product = productOptional.get();
        product.setDailyLimit(request.getDailyLimit());
        product.setUpdatedAt(updatedTime);
        productRepository.save(product);

        ProductEvent productEvent = new ProductEvent();
        productEvent.setProductId(product.getId());
        productEvent.setProductCode(product.getProductCode());
        productEvent.setProductLimitAmount(product.getDailyLimit());
        productEvent.setProductLimitType("DAILY_LIMIT");
        productEvent.setEventCreatedAt(updatedTime);
        productEvent.setProductLastUpdatedAt(updatedTime);
        productEventService.createProductLimitEvent(productEvent);

        return productOptional;
    }
}
